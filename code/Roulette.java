import java.util.Scanner;
public class Roulette{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);

        RouletteWheel wheel = new RouletteWheel();
        int money = 1000;

        System.out.println("Would you like to make a bet?");
        String q1Answer = scan.nextLine();

        System.out.println();

        if(q1Answer.equals("yes") || q1Answer.equals("y")){
            System.out.println("What number would you like to bet on?");
            int q2Answer = scan.nextInt();

            System.out.println();

            System.out.println("How much would you like to bet?");
            int q3Answer = scan.nextInt();

            System.out.println();

            while(q3Answer < 0 || q3Answer > money){
                System.out.println("You cannot bet more money than you already have!");
                System.out.println("Please try again.");
                q3Answer = scan.nextInt();
            }

            wheel.spin();

            System.out.println();

            int num = wheel.getValue();
            if(num == q2Answer){
                System.out.println("You won!");
                int moneyWon = q3Answer * 35;
                System.out.println("You won " +moneyWon+ "$!!!");
            }
            else{
                System.out.println("Sorry, you did not win! Better luck next time!");
            }
        }

       


    }

}