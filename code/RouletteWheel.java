import java.util.Random;
public class RouletteWheel {
    private Random generator;
    private int numLastSpin;

    public RouletteWheel(){
        this.generator = new Random();
        this.numLastSpin = 0;
    }

    public void spin(){
        this.numLastSpin = generator.nextInt(37);
        System.out.println("Wheel landed on " +this.numLastSpin);
    }

    public int getValue(){
        return this.numLastSpin;
    }
}
